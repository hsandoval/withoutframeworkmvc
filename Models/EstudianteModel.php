<?php
	include_once('DireccionModel.php');

	class Estudiante
	{
		public $Nombre;
		public $Edad;
		public $Direccion;

		function __construct($name, $age, $address) {
			$this->Nombre = $name;
			$this->Edad = $age;
			$this->Direccion = new Direccion($address->City, $address->Street);
		}
	}
