<?php
	class HomeController
	{
		public function __construct(){}

		public function Index(){		
			require_once('Views/Home/Index.php');
		}
		
		public function	Error(){
			require_once('Views/Home/Error.php');
		}
	}