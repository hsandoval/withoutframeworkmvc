<?php
	require_once('Models/EstudianteModel.php');

	class EstudianteController
	{	
		public function __construct(){}

		public function Index(){		
			require_once('Views/Estudiante/Index.php');
		}

		public function	ObtenerTodos(){
			$arrayEstudiantes = array();

			$data = file_get_contents('estudiantes.json');
			$estudiantes = json_decode($data);
		
			foreach ($estudiantes as $key => $value) {
				array_push($arrayEstudiantes, new Estudiante($value->Name, $value->Age, $value->Address));
			}

			var_dump($estudiantes);
		}
	}