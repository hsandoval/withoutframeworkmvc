<?php
	function DesplegarVista($controller, $action){
		require_once('Controllers/' . $controller . 'Controller.php');

		switch($controller){
			case 'Home':
				$controller= new HomeController();
				break; 
			case 'Estudiante':
				$controller= new EstudianteController();
				break;
		}

		$controller->{ $action }();
	}

	function DesplegarVistaError($exception) {
		DesplegarVista('Home', 'Error');
	}
	
	$controllers= array(
		'Home'=> [
			'Index',
			'Error'
		],
        'Estudiante'=> [
			'Index',
			'ObtenerTodos'
			]
		);
		
	try {
		if (array_key_exists($controller, $controllers)) {
			//verifica que el arreglo controllers con la clave que es la variable controller del index exista la acción
			if (in_array($action, $controllers[$controller])) {
				//llama  la función call y le pasa el controlador a llamar y la acción (método) que está dentro del controlador
				DesplegarVista($controller, $action);
			} else {
				throw new Exception("La acción {$action}, no está definida en el controlador {$controller}.", 1);
			}
		} else {
			throw new Exception("El controlador {$controller}, no está definido.", 1);
		}
	} catch (Exception $exception) {
		DesplegarVistaError($exception);
	}
?>