<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Titulo documento</title>
    <link rel="stylesheet" href="wwwroot/css/style.css">
</head>
<body>
	<div class="wrapper">
        <div class="header"><img src="wwwroot/img/iujo.png" alt="Logotipo IUJO" ></div>
        <div class="sidebar">Sidebar
            <nav class="main-nav">
                <ul>
                    <li><a href="Controllers/EstudianteController.php?action=Index">Listar</a></li>
                </ul>
            </nav>            
        </div>
        <div class="footer">
            <p>© <?php echo(getdate()['year']) ?> Todos los derechos reservados </p>
        </div>
        
        <div class="content">Content
            <?php require_once('Routes.php'); ?>
        </div>
    </div>  
</body>
</html>